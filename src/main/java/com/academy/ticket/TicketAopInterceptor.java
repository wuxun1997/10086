package com.academy.ticket;


import com.academy.utils.ExpiredCache;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 隔壁老王在练腰，你的情人在磨刀，
 * 加油 , 望有朝一日，走路生风，跨步起云，踏浪无痕。
 */

@Component
@Slf4j
public class TicketAopInterceptor extends HandlerInterceptorAdapter {
    private final static SimpleDateFormat SF = new SimpleDateFormat("yyyyMMddHHmmss");
    // 话单格式：请求时间|接口名称|接口时延|调用方IP|本地IP|图片url|结果码|qps
    private final static String CDR_FORMAT = "{}|{}|{}|{}|{}|{}|{}|{}";
    // 时间戳缓存
//    private final static TimeStampList cache = new TimeStampList(10000);
    private ExpiredCache expiredCache = new ExpiredCache();

    private static final ThreadLocal<Map<String, String>> threadLocal = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 记录时间
        String beginTime = String.valueOf(System.currentTimeMillis());
        String interfaceName = request.getServletPath();
        String requestpath = interfaceName.substring(interfaceName.lastIndexOf("/") + 1);
        // 调用方ip
        String remoteAddr = request.getRemoteAddr();
        //本地ip
        String localAddr = request.getLocalAddr();
        // 获取qps：每秒请求的并发量
        long qps = expiredCache.set(interfaceName, 1).get();
        String imageUrl = request.getParameter("imageUrl");
        // 获取json字符串
        if (isJson(request)) {
            // 获取json字符串
            String jsonParam = new RequestWrapper(request).getBodyString();
            JSONObject jsonObject = JSON.parseObject(jsonParam);
            imageUrl = jsonObject.getString("imageUrl");
        }
        Map<String, String> strMap = new HashMap<>();
        strMap.put("beginTime", beginTime);
        strMap.put("interfaceName", requestpath);
        strMap.put("remoteIp", remoteAddr);
        strMap.put("localIp", localAddr);
        strMap.put("imageUrl", imageUrl);
        strMap.put("qps", String.valueOf(qps));
        threadLocal.set(strMap);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        Map<String, String> strMap = threadLocal.get();
        long beginTime = Long.parseLong(strMap.get("beginTime"));
        long currentTime = System.currentTimeMillis();
        String currentDate = SF.format(new Date(currentTime));
        int status = response.getStatus();

        // 成功失败都应该记话单
//        if (status == 200) {
        strMap.put("status", String.valueOf(status));
        // 记录话单
        // 话单格式：请求时间|接口名称|接口时延|调用方IP|本地IP|图片url|结果码
        log.error(CDR_FORMAT, currentDate, strMap.get("interfaceName"), currentTime - beginTime, strMap.get("remoteIp"),
                strMap.get("localIp"), strMap.get("imageUrl"), strMap.get("status"), strMap.get("qps"));
//        }
    }

    /**
     * 判断本次请求的数据类型是否为json
     *
     * @param request
     * @return boolean
     */
    private boolean isJson(HttpServletRequest request) {
        if (request.getContentType() != null) {
            return request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE) ||
                    request.getContentType().equals(MediaType.APPLICATION_JSON_UTF8_VALUE);
        }

        return false;
    }


}
