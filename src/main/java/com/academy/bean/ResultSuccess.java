package com.academy.bean;

public class ResultSuccess<T> extends ResultError {
    private T data;

    public T getData() {
        return data;
    }

    public ResultSuccess(String code, String message, T data) {
        super(code, message);
        this.data = data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
