package com.academy.ticket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;


@Configuration
public class InterceptorConfig extends WebMvcConfigurationSupport {
    @Autowired
    private TicketAopInterceptor ticketAopInterceptor;
    @Bean
    public TicketAopInterceptor getSignatureInterceptor(){
        return new TicketAopInterceptor();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 拦截器重复注册
//        registry.addInterceptor(getSignatureInterceptor())
//                .addPathPatterns("/**");
        // 如果配置项目名，则拦截项目后面的地址，
        //比如配置访问项目名地址为  springboot,则拦截的是"localhost:8080/springboot/hello" 后面的地址
        // 如果没有配置项目名，则拦截地址为 "localhost/hello" 后面的地址
        registry.addInterceptor(ticketAopInterceptor).addPathPatterns("/carNoRec");
        registry.addInterceptor(ticketAopInterceptor).addPathPatterns("/graduationCertRec");
        registry.addInterceptor(ticketAopInterceptor).addPathPatterns("/identifyCardRec");
    }
}
